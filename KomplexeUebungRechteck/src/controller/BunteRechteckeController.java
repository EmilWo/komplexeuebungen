package controller;

import java.util.LinkedList;
import java.util.List;

import model.MYSQLDatabase;
import model.Rechteck;

public class BunteRechteckeController {
	private List rechtecke;
	private MYSQLDatabase database;

	public BunteRechteckeController() {
		super();
		rechtecke = new LinkedList();
		this.database = new MYSQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}

	public List getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteckEintragen(rechteck);
	}

	public void reset() {
		rechtecke.clear();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckHinzufuegen() {
		new RechteckneuGUI();
	}
}