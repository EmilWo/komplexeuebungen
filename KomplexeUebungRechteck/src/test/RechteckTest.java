package test;

import model.Rechteck;

public class RechteckTest {
	Rechteck rechteck0 = new Rechteck(10, 10, 30, 40);
	Rechteck rechteck1 = new Rechteck(25, 25, 100, 20);
	Rechteck rechteck2 = new Rechteck(260, 10, 200, 100);
	Rechteck rechteck3 = new Rechteck(5, 500, 300, 25);
	Rechteck rechteck4 = new Rechteck(100, 100, 100, 100);
	Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
	Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
	Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
	Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
	Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rechteck0 == null) ? 0 : rechteck0.hashCode());
		result = prime * result + ((rechteck1 == null) ? 0 : rechteck1.hashCode());
		result = prime * result + ((rechteck2 == null) ? 0 : rechteck2.hashCode());
		result = prime * result + ((rechteck3 == null) ? 0 : rechteck3.hashCode());
		result = prime * result + ((rechteck4 == null) ? 0 : rechteck4.hashCode());
		result = prime * result + ((rechteck5 == null) ? 0 : rechteck5.hashCode());
		result = prime * result + ((rechteck6 == null) ? 0 : rechteck6.hashCode());
		result = prime * result + ((rechteck7 == null) ? 0 : rechteck7.hashCode());
		result = prime * result + ((rechteck8 == null) ? 0 : rechteck8.hashCode());
		result = prime * result + ((rechteck9 == null) ? 0 : rechteck9.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RechteckTest other = (RechteckTest) obj;
		if (rechteck0 == null) {
			if (other.rechteck0 != null)
				return false;
		} else if (!rechteck0.equals(other.rechteck0))
			return false;
		if (rechteck1 == null) {
			if (other.rechteck1 != null)
				return false;
		} else if (!rechteck1.equals(other.rechteck1))
			return false;
		if (rechteck2 == null) {
			if (other.rechteck2 != null)
				return false;
		} else if (!rechteck2.equals(other.rechteck2))
			return false;
		if (rechteck3 == null) {
			if (other.rechteck3 != null)
				return false;
		} else if (!rechteck3.equals(other.rechteck3))
			return false;
		if (rechteck4 == null) {
			if (other.rechteck4 != null)
				return false;
		} else if (!rechteck4.equals(other.rechteck4))
			return false;
		if (rechteck5 == null) {
			if (other.rechteck5 != null)
				return false;
		} else if (!rechteck5.equals(other.rechteck5))
			return false;
		if (rechteck6 == null) {
			if (other.rechteck6 != null)
				return false;
		} else if (!rechteck6.equals(other.rechteck6))
			return false;
		if (rechteck7 == null) {
			if (other.rechteck7 != null)
				return false;
		} else if (!rechteck7.equals(other.rechteck7))
			return false;
		if (rechteck8 == null) {
			if (other.rechteck8 != null)
				return false;
		} else if (!rechteck8.equals(other.rechteck8))
			return false;
		if (rechteck9 == null) {
			if (other.rechteck9 != null)
				return false;
		} else if (!rechteck9.equals(other.rechteck9))
			return false;
		return true;
	}

}